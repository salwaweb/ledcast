jQuery(document).ready(function ()
{
	// =====================================================
	// LazyLoad
	// =====================================================
		 jQuery(".lazyload").lazyload({
	 	 event: "lazyload",
	 	 container: jQuery(".catalogue_bloc_main"),
         data_attribute : "src",
         threshold : 50,
         effect : "fadeIn"
     });
	
	nWidth = jQuery(document).width();
	if (nWidth < 720)
	{
		jQuery('.catalogue_bloc_context_item ul').slideUp();
		jQuery('.catalogue_bloc_context_item h2').addClass('slide_up');
		jQuery('.catalogue_bloc_context_item h2').removeClass('slide_down');
	}
	else
	{
		jQuery('.catalogue_bloc_context_item ul').slideDown();
		jQuery('.catalogue_bloc_context_item h2').removeClass('slide_up');
		jQuery('.catalogue_bloc_context_item h2').addClass('slide_down');
	}

	jQuery(window).on('resize', function ()
	{
		nWidth = jQuery(document).width();
		if (nWidth < 720)
		{
			jQuery('.catalogue_bloc_context_item ul').slideUp();
			jQuery('.catalogue_bloc_context_item h2').addClass('slide_up');
			jQuery('.catalogue_bloc_context_item h2').removeClass('slide_down');
		}
		else
		{
			jQuery('.catalogue_bloc_context_item ul').slideDown();
			jQuery('.catalogue_bloc_context_item h2').removeClass('slide_up');
			jQuery('.catalogue_bloc_context_item h2').addClass('slide_down');

		}
	});


	jQuery('.catalogue_bloc_context_item h2').on('click', function () {
		nWidth = jQuery(document).width();
		if (nWidth < 720)
		{
			jQuery(this).parent().find('ul').slideToggle();
			jQuery(this).toggleClass('slide_up');
			jQuery(this).toggleClass('slide_down');
		}
	});



//	jQuery('.filter_listaction_inner h4').live('click', function (e)
	jQuery(document).on('click', '.filter_listaction_inner h4', function (e)
	{


		if (jQuery('.filter_listaction_inner h4').hasClass('clicked'))
		{
			jQuery('.filter_listaction_inner h4').removeClass('clicked');
			jQuery('#category_submenu_inner').removeClass('clicked');
			jQuery('.filter_action').removeClass('clicked');
		}
		else
		{
			jQuery('.filter_listaction_inner h4').addClass('clicked');
			jQuery('#category_submenu_inner').addClass('clicked');
			jQuery('.filter_action').addClass('clicked');
		}
	});
	
	
	// =====================================================
	// Back to top
	// =====================================================
	jQuery(document).on("click", "#catalogue_list_backtop", function()
	{
     	jQuery('html, body').animate(
		{
			scrollTop:0
		}, 'slow');
		return false;
	});
	
});