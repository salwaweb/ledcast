function moveGaleryItem(psSourceId, psSourceType, pnSourcePK, pnDestinationPK)
{
	if (psSourceType == 'folder')
	{
		var sAction = 'move_folder';
		if (pnSourcePK == pnDestinationPK)
		{
			alert('Vous ne pouvez pas déplacer un répertoire sur lui même');
			return false;
		}
	}	
	else
		var sAction = 'move_item';
		
	$(psSourceId).hide();	
		
	var sUrl = 'index.php?page=mgaleryback&action=ajax&ajax=' + sAction + '&key=' + pnSourcePK + '&destination=' + pnDestinationPK;
	
	new Ajax.Request(sUrl, 
		{
			method: 'get',
			onSuccess: function(transport) 
			{
				var nResult = parseInt(transport.responseText);
				
				if (nResult == -1)
				{
					$(psSourceId).show();	
					alert('Impossible de déplacer un dossier dans un de ses dossiers fils');
				}	
				else if (nResult == 1)
				{
					$(psSourceId).remove();	
				}
				
				else
				{
					$(psSourceId).show();
					alert("Echec lors du déplacement de l'élément");
				}
			}
		}	
	);	
}