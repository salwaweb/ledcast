<?php

namespace LedcastBundle\Controller;

use LedcastBundle\Entity\Actualite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LedcastController extends Controller
{
    public function a_proposAction()
    {
        return $this->render('LedcastBundle:ledcast:a-propos.html.twig');
    }

    public function actualiteAction()
    {
        $em = $this->getDoctrine()->getManager();

        $actualites = $em->getRepository('LedcastBundle:Actualite')->findBy(
    array(),
    array('id' => 'DESC')
);

        return $this->render('LedcastBundle:ledcast:actualites.html.twig', array(
            'actualites' => $actualites,
        ));

    }
    /**
     * Finds and displays a actualite entity.
     *
     */

    public function newsAction(Actualite $actualite)
    {
        return $this->render('LedcastBundle:ledcast:news.html.twig', array(
            'actualite' => $actualite,

        ));
    }

    public function bureauAction()
    {
        return $this->render('LedcastBundle:ledcast:bureau-etude.html.twig');
    }

    public function crossAction()
    {
        return $this->render('LedcastBundle:ledcast:cross-rental.html.twig');
    }

    public function formationAction()
    {
        return $this->render('LedcastBundle:ledcast:formation.html.twig');
    }

}