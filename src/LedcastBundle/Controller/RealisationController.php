<?php

namespace LedcastBundle\Controller;
use LedcastBundle\Entity\Categorie;
use LedcastBundle\Entity\Realisation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Realisation controller.
 *
 */
class RealisationController extends Controller
{
    /**
     * Lists all realisation entities.
     *
     */
    public function affichageAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cat = $em->getRepository('LedcastBundle:Categorie')->findById(5);
        //dd($cat);
        $realisations = $em->getRepository('LedcastBundle:Realisation')->findBy(array('Categorie' => $cat));
        //dd($realisations);
        return $this->render('LedcastBundle:Realisation:affichage-urbain.html.twig', array(

            'categorie' => $cat,
            'realisations' => $realisations
        ));
    }

    public function commerceAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cat = $em->getRepository('LedcastBundle:Categorie')->findById(1);
        //dd($cat);
        $realisations = $em->getRepository('LedcastBundle:Realisation')->findBy(array('Categorie' => $cat));
        //dd($realisations);
        return $this->render('LedcastBundle:Realisation:commerce-retail.html.twig', array(
            'categorie' => $cat,
            'realisations' => $realisations

        ));
    }

    public function evenementielAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cat = $em->getRepository('LedcastBundle:Categorie')->findById(3);
        //dd($cat);
        $realisations = $em->getRepository('LedcastBundle:Realisation')->findBy(array('Categorie' => $cat));
        //dd($realisations);
        return $this->render('LedcastBundle:Realisation:evenementiels.html.twig', array(
            'categorie' => $cat,
            'realisations' => $realisations

        ));

    }

    public function institutionelAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cat = $em->getRepository('LedcastBundle:Categorie')->findById(2);
        //dd($cat);
        $realisations = $em->getRepository('LedcastBundle:Realisation')->findBy(array('Categorie' => $cat));
        //dd($realisations);
        return $this->render('LedcastBundle:Realisation:institutionnel.html.twig', array(
            'categorie' => $cat,
            'realisations' => $realisations

        ));

    }

    public function sportAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cat = $em->getRepository('LedcastBundle:Categorie')->findById(4);
        //dd($cat);
        $realisations = $em->getRepository('LedcastBundle:Realisation')->findBy(array('Categorie' => $cat));
        //dd($realisations);
        return $this->render('LedcastBundle:Realisation:sports.html.twig', array(
            'categorie' => $cat,
            'realisations' => $realisations

        ));

    }
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $realisations = $em->getRepository('LedcastBundle:Realisation')->findAll();

        return $this->render('realisation/index.html.twig', array(
            'realisations' => $realisations,
        ));
    }

    /**
     * Creates a new realisation entity.
     *
     */
    public function newAction(Request $request)
    {
        $realisation = new Realisation();
        $form = $this->createForm('LedcastBundle\Form\RealisationType', $realisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($realisation);
            $em->flush($realisation);

            return $this->redirectToRoute('realisation_show', array('id' => $realisation->getId()));
        }

        return $this->render('realisation/new.html.twig', array(
            'realisation' => $realisation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a realisation entity.
     *
     */
    public function showAction(Realisation $realisation)
    {
        $deleteForm = $this->createDeleteForm($realisation);

        return $this->render('realisation/show.html.twig', array(
            'realisation' => $realisation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing realisation entity.
     *
     */
    public function editAction(Request $request, Realisation $realisation)
    {
        $deleteForm = $this->createDeleteForm($realisation);
        $editForm = $this->createForm('LedcastBundle\Form\RealisationType', $realisation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('realisation_edit', array('id' => $realisation->getId()));
        }

        return $this->render('realisation/edit.html.twig', array(
            'realisation' => $realisation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a realisation entity.
     *
     */
    public function deleteAction(Request $request, Realisation $realisation)
    {
        $form = $this->createDeleteForm($realisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($realisation);
            $em->flush($realisation);
        }

        return $this->redirectToRoute('realisation_index');
    }

    /**
     * Creates a form to delete a realisation entity.
     *
     * @param Realisation $realisation The realisation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Realisation $realisation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('realisation_delete', array('id' => $realisation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
