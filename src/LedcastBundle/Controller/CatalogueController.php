<?php

namespace LedcastBundle\Controller;

use LedcastBundle\Entity\Catalogue;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Catalogue controller.
 *
 */
class CatalogueController extends Controller
{
    /**
     * Lists all catalogue entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catalogues = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('catalogue/index.html.twig', array(
            'catalogues' => $catalogues,
        ));
    }
    public function produitsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('LedcastBundle:Default:produits.html.twig', array(
            'categories' => $categories,
        ));

    }

    /**
     * Finds and displays a catalogue entity.
     *
     */
    public function showAction(Request $request,Catalogue $categorie)
    {
		 $em = $this->getDoctrine()->getManager();       
		$produits = $em->getRepository('LedcastBundle:Produit')->findBy(
			array(),
			array('pitchp' => 'ASC')
			
		);
		
		 $produitsGrouped = $em->createQueryBuilder()
			->select('p')
			->from('LedcastBundle:Produit', 'p')
			->groupBy('p.type')
			->addGroupBy('p.categorie')
			->add('orderBy', "p.nom ASC")
			->getQuery();
			
			
	
        $pitchs = $em->getRepository('LedcastBundle:Pitch')->findBy(
			array(),
			array('titre' => 'ASC')
		);
        $types = $em->getRepository('LedcastBundle:Type')->findAll();
        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('LedcastBundle:Default:shownew.html.twig', array(
            'produits' => $produits,
            'produitsGrouped' => $produitsGrouped->getResult(),
            'pitchs' => $pitchs,
            'types' => $types,
            'categorie' => $categorie,
            'categories' => $categories,
        ));
       
    }
	
	/**
     * Finds and displays a catalogue entity.
     *
     */
    public function shownewAction(Request $request,Catalogue $categorie)
    {
 $em = $this->getDoctrine()->getManager();
        /*$type=$request->get('type');
        $pitch=$request->get('pitch');
        //$categories=$request->get('categorie');
        //var_dump();die;
        if($categorie == null and $pitch==null and $type==null)
        {
            $produits = $em->getRepository('LedcastBundle:Produit')->findAll();
        }
        else {
            $produits = $em->getRepository('LedcastBundle:Produit')->findBy(array(
                'pitch' => $pitch,

                'categorie' => $categorie,
                'type' => $type

            ));
        }*/
        
		$produits = $em->getRepository('LedcastBundle:Produit')->findBy(
			array(),
			array('pitchp' => 'ASC'),50, 0
			
		);
		
		 $produitsGrouped = $em->createQueryBuilder()
			->select('p')
			->from('LedcastBundle:Produit', 'p')
			->groupBy('p.typeled')
			->add('orderBy', "p.pitchp ASC")
			->setMaxResults(50)
			->getQuery();
	
        $pitchs = $em->getRepository('LedcastBundle:Pitch')->findBy(
			array(),
			array('titre' => 'ASC')
		);
        $types = $em->getRepository('LedcastBundle:Type')->findAll();
        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('LedcastBundle:Default:prod.html.twig', array(
            'produits' => $produits,
            //'produitsGrouped' => $produitsGrouped->getResult(),
            'pitchs' => $pitchs,
            'types' => $types,
            'categorie' => $categorie,
            'categories' => $categories,
        ));
       
    }


    /**
     * Creates a new catalogue entity.
     *
     */
    public function newAction(Request $request)
    {
        $catalogue = new Catalogue();
        $form = $this->createForm('LedcastBundle\Form\CatalogueType', $catalogue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($catalogue);
            $em->flush();

            return $this->redirectToRoute('catalogue_show', array('id' => $catalogue->getId()));
        }

        return $this->render('catalogue/new.html.twig', array(
            'catalogue' => $catalogue,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing catalogue entity.
     *
     */
    public function editAction(Request $request, Catalogue $catalogue)
    {
        $deleteForm = $this->createDeleteForm($catalogue);
        $editForm = $this->createForm('LedcastBundle\Form\CatalogueType', $catalogue);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('catalogue_edit', array('id' => $catalogue->getId()));
        }

        return $this->render('catalogue/edit.html.twig', array(
            'catalogue' => $catalogue,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a catalogue entity.
     *
     */
    public function deleteAction(Request $request, Catalogue $catalogue)
    {
        $form = $this->createDeleteForm($catalogue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($catalogue);
            $em->flush();
        }

        return $this->redirectToRoute('catalogue_index');
    }

    /**
     * Creates a form to delete a catalogue entity.
     *
     * @param Catalogue $catalogue The catalogue entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Catalogue $catalogue)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catalogue_delete', array('id' => $catalogue->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
