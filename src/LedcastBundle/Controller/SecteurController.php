<?php

namespace LedcastBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecteurController extends Controller
{
    public function affichageAction()
    {
        return $this->render('LedcastBundle:secteur:affichage-urbain.html.twig');
    }

    public function commerceAction()
    {
        return $this->render('LedcastBundle:secteur:commerce-retail.html.twig');
    }

    public function evenementielAction()
    {
        return $this->render('LedcastBundle:secteur:evenementiels.html.twig');
    }

    public function institutionelAction()
    {
        return $this->render('LedcastBundle:secteur:institutionnel.html.twig');
    }

    public function sportAction()
    {
        return $this->render('LedcastBundle:secteur:sports.html.twig');
    }

}