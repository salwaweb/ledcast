<?php

namespace LedcastBundle\Controller;
use LedcastBundle\Entity\Produit;
use LedcastBundle\Entity\Contact;
use LedcastBundle\Entity\Accessoir;
use LedcastBundle\Entity\Actualite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('LedcastBundle:Produit')->findBy(array(),array('id'=>'desc'),3);
        $actualites = $em->getRepository('LedcastBundle:Actualite')->findBy(array(),array('id'=>'desc'),4);
		$dejaVu = $request->cookies->has("popup_first_visit");

        $response = $this->render('LedcastBundle:Default:index.html.twig', array(
            'produits' => $produits,
            'actualites' => $actualites
        ));
        
        if($dejaVu)
        {
            $cookie_info = array(
                'name'  => 'popup_first_visit',
                'value' => new \DateTime('now'),
                'time'  => time() + 3600 * 24 * 7
            );

            // Cree le cookie
            $cookie = new Cookie($cookie_info['name'], $cookie_info['value'], $cookie_info['time']);

            // Envoie le cookie
            
            $response->headers->setCookie($cookie);
        }

        return $response->send();
    }
	
	
	
    public function ledcastAction()
    {
        return $this->render('LedcastBundle:Default:ledcast.html.twig');
    }
     public function legalAction()
    {
        return $this->render('LedcastBundle:Default:legal.html.twig');
    }


    public function secteurAction()
    {
        return $this->render('LedcastBundle:Default:secteur-activite.html.twig');
    }
    public function supportAction()
    {
        return $this->render('LedcastBundle:Default:support.html.twig');
    }

    public function realisationAction()
    {
        return $this->render('LedcastBundle:Default:realisation.html.twig');
    }
    public function locationAction()
    {
        return $this->render('LedcastBundle:Default:location.html.twig');
    }
    public function testAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('LedcastBundle:Produit')->findAll();
        $pitchs = $em->getRepository('LedcastBundle:Pitch')->findAll();
        $types = $em->getRepository('LedcastBundle:Type')->findAll();
        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('LedcastBundle:Default:categorie.html.twig', array(
            'produits' => $produits,
            'pitchs' => $pitchs,
            'types' => $types,
            'categories' => $categories,
        ));

    }
    public function catalogueAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('LedcastBundle:Produit')->findBy(
    array(),
    array('pitchp' => 'ASC')
);
        $pitchs = $em->getRepository('LedcastBundle:Pitch')->findAll();
        $types = $em->getRepository('LedcastBundle:Type')->findAll();
        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('LedcastBundle:Default:catalogue.html.twig', array(
            'produits' => $produits,
            'pitchs' => $pitchs,
            'types' => $types,
            'categories' => $categories,
        ));

    }

    public function categorie1Action(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $type=$request->get('type');
        $pitch=$request->get('pitch');
        $categorie=$request->get('categorie');
        //var_dump();die;
        if($categorie == null and $pitch==null and $type==null)
        {
            $produits = $em->getRepository('LedcastBundle:Produit')->findAll();
        }
        else {
            $produits = $em->getRepository('LedcastBundle:Produit')->findBy(array(
                'pitch' => $pitch,
                'categorie' => $categorie,
                'type' => $type

            ));
        }
        $pitchs = $em->getRepository('LedcastBundle:Pitch')->findAll();
        $types = $em->getRepository('LedcastBundle:Type')->findAll();
        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();

        return $this->render('LedcastBundle:Default:catalogue.html.twig', array(
            'produits' => $produits,
            'pitchs' => $pitchs,
            'types' => $types,
            'categories' => $categories,
        ));

    }
    public function productAction(Produit $produit)
    {
         $em = $this->getDoctrine()->getManager();

       $points = $em->getRepository('LedcastBundle:AccessoirPointfort')->findAll();
        //dd($point);
       $qb = $em->createQueryBuilder();
        $qb->select('a')
            ->from('LedcastBundle:Accessoir', 'a')
            //->where('a.pointfort.id = ?prd');
            ->distinct('a.id');
        $query = $qb->getQuery();

        $access= $query->getResult();
//dd($access);

        $accessoirs = $em->getRepository('LedcastBundle:Accessoir')->findAll();
        $produits = $em->getRepository('LedcastBundle:Produit')->findAll(); 
        $categories = $em->getRepository('LedcastBundle:Catalogue')->findAll();
        return $this->render('LedcastBundle:Default:product.html.twig', array(
            'points' => $points,
            'produit' => $produit,
            'accessoirs' => $accessoirs,
            'produits' => $produits,
            'categories' => $categories,

        ));

    }
       public function accessoirAction(Accessoir $accessoir)
    {
        $em = $this->getDoctrine()->getManager();

        $accessoirs = $em->getRepository('LedcastBundle:Accessoir')->findAll();
        //dd($accessoir);

        return $this->render('LedcastBundle:Default:accessoir.html.twig', array(

            'accessoir' => $accessoir,
            'accessoirs' => $accessoirs,


        ));

    }


    public function contactAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm('LedcastBundle\Form\ContactType', $contact);
        $form->handleRequest($request);

if($form->isSubmitted() &&  $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))){

            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $nom=$contact->getNom();
            $prenom=$contact->getPrenom();
            $societe=$contact->getSociete();
            $telephone=$contact->getTelephone();
            $email=$contact->getEmail();
            $sujet=$contact->getSujet();
            $produit=$contact->getProduit();
            $body=$contact->getMessage();
            $mail = \Swift_Message::newInstance()
                ->setSubject('date rendez vous  ')
                ->setFrom('lasociete.ledcast@gmail.com')
                ->setTo('contact@ledcast.fr')

                ->setBody(
                    $this->renderView( 'LedcastBundle:Default:m.html.twig',
                        array('nom'  => $nom,
                            'prenom'  => $prenom,
                            'societe'  => $societe,
                            'email'  => $email,
                            'telephone'  => $telephone,
                            'sujet'  => $sujet,
                            'produit'  => $produit,
                            'body'  => $body,
                            ))
                    ,
                    'text/html'
                );
            $this->get('mailer')->send($mail);
			 if($mail==true){
                echo "Email envoyer avec success ";
            } else{
                echo "Erreur d'envoyer l'email";
            }

            return $this->redirectToRoute('contact');
        }
	# check if captcha response isn't get throw a message
            if($form->isSubmitted() &&  $form->isValid() && !$this->captchaverify($request->get('g-recaptcha-response'))){
                 
            $this->addFlash(
                'error',
                'Captcha Require'
              );             
            }
        return $this->render('LedcastBundle:Default:contact.html.twig', array(
            'contact' => $contact,
            'form' => $form->createView(),
        ));
    }
	 # get success response from recaptcha and return it to controller
    function captchaverify($recaptcha){
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                "secret"=>"6LcDUJQUAAAAAFL-25TGTUtjXHBatBquCNe9PsQY","response"=>$recaptcha));
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);     
        
        return $data->success;        
    }

}
