<?php

namespace LedcastBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SupportController extends Controller
{


    public function faqAction()
    {
        return $this->render('LedcastBundle:support:faq.html.twig');
    }

    public function garantieAction()
    {
        return $this->render('LedcastBundle:support:garantie.html.twig');
    }

    public function telechargementAction()
    {
        return $this->render('LedcastBundle:support:telechargement.html.twig');
    }



}