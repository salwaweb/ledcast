<?php

namespace LedcastBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prenom')
            ->add('nom')
            ->add('societe')
            ->add('telephone')
            ->add('email')

            ->add('sujet', ChoiceType::class, array(
                'choices'  => array(
                    'Demande d`informations' => 'Demande d`informations',
                     'Demande de RDV showroom' => 'Demande de RDV showroom',
                     'Cross-rental' => 'Cross-rental',
                     'Formation LedCast' => 'Formation LedCast',
                     'Autre' => 'Autre',

                ),
            ))
            ->add('produit')
            ->add('message');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LedcastBundle\Entity\Contact'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ledcastbundle_contact';
    }


}
