<?php

namespace LedcastBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
            ->add('slug')
            ->add('description')
            ->add('prix')
            ->add('pitch')
            ->add('typeled')
            ->add('taille')
            ->add('resolution')
            ->add('densite')
            ->add('luminosit')
            ->add('frequence')
            ->add('dimensions')
            ->add('resolutiond')
            ->add('chassis')
            ->add('maintenance')
            ->add('poids')
            ->add('consommation')
            ->add('anglevh')
            ->add('distancem')
            ->add('carterec')
            ->add('temperateur')
            ->add('tolerance')
            ->add('refroidissement')
            ->add('indicepro')
            ->add('connections')
            ->add('certification')
            ->add('duree')
            ->add('garantie')
            ->add('categorie')
            ->add('typep')
            ->add('pitchp')
            ->add('image1')
            ->add('image2')
            ->add('image3')
            ->add('fichier1')

        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LedcastBundle\Entity\Produit'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ledcastbundle_produit';
    }


}
