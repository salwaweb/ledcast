<?php

namespace LedcastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pointfort
 *
 * @ORM\Table(name="pointfort")
 * @ORM\Entity(repositoryClass="LedcastBundle\Repository\PointfortRepository")
 */
class Pointfort
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;
    /**
     * @var string
     *
     * @ORM\Column(name="point1", type="string", length=255)
     */
    private $point1;

    /**
     * @var string
     *
     * @ORM\Column(name="point2", type="string", length=255)
     */
    private $point2;

    /**
     * @var string
     *
     * @ORM\Column(name="point3", type="string", length=255)
     */
    private $point3;

    /**
     * @var string
     *
     * @ORM\Column(name="point4", type="string", length=255)
     */
    private $point4;

    /**
     * @var string
     *
     * @ORM\Column(name="point5", type="string", length=255)
     */
    private $point5;
    /**
     * @var string
     *
     * @ORM\Column(name="point6", type="string", length=255)
     */
    private $point6;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fiche;
    /**
     * @ORM\OneToMany(targetEntity="LedcastBundle\Entity\Pointfort", mappedBy="pointf")
     * @ORM\JoinColumn(nullable=true)
     */

    private $fortp;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set point1
     *
     * @param string $point1
     *
     * @return Pointfort
     */
    public function setPoint1($point1)
    {
        $this->point1 = $point1;

        return $this;
    }

    /**
     * Get point1
     *
     * @return string
     */
    public function getPoint1()
    {
        return $this->point1;
    }

    /**
     * Set point2
     *
     * @param string $point2
     *
     * @return Pointfort
     */
    public function setPoint2($point2)
    {
        $this->point2 = $point2;

        return $this;
    }

    /**
     * Get point2
     *
     * @return string
     */
    public function getPoint2()
    {
        return $this->point2;
    }

    /**
     * Set point3
     *
     * @param string $point3
     *
     * @return Pointfort
     */
    public function setPoint3($point3)
    {
        $this->point3 = $point3;

        return $this;
    }

    /**
     * Get point3
     *
     * @return string
     */
    public function getPoint3()
    {
        return $this->point3;
    }

    /**
     * Set point4
     *
     * @param string $point4
     *
     * @return Pointfort
     */
    public function setPoint4($point4)
    {
        $this->point4 = $point4;

        return $this;
    }

    /**
     * Get point4
     *
     * @return string
     */
    public function getPoint4()
    {
        return $this->point4;
    }

    /**
     * Set point5
     *
     * @param string $point5
     *
     * @return Pointfort
     */
    public function setPoint5($point5)
    {
        $this->point5 = $point5;

        return $this;
    }

    /**
     * Get point5
     *
     * @return string
     */
    public function getPoint5()
    {
        return $this->point5;
    }

    /**
     * Set point6
     *
     * @param string $point6
     *
     * @return Pointfort
     */
    public function setPoint6($point6)
    {
        $this->point6 = $point6;

        return $this;
    }

    /**
     * Get point6
     *
     * @return string
     */
    public function getPoint6()
    {
        return $this->point6;
    }

    /**
     * Set fiche
     *
     * @param \LedcastBundle\Entity\Media $fiche
     *
     * @return Pointfort
     */
    public function setFiche(\LedcastBundle\Entity\Media $fiche = null)
    {
        $this->fiche = $fiche;

        return $this;
    }

    /**
     * Get fiche
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getFiche()
    {
        return $this->fiche;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Pointfort
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->titre;
        // to show the id of the Category in the select
        // return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fortp = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add fortp
     *
     * @param \LedcastBundle\Entity\Pointfort $fortp
     *
     * @return Pointfort
     */
    public function addFortp(\LedcastBundle\Entity\Pointfort $fortp)
    {
        $this->fortp[] = $fortp;

        return $this;
    }

    /**
     * Remove fortp
     *
     * @param \LedcastBundle\Entity\Pointfort $fortp
     */
    public function removeFortp(\LedcastBundle\Entity\Pointfort $fortp)
    {
        $this->fortp->removeElement($fortp);
    }

    /**
     * Get fortp
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFortp()
    {
        return $this->fortp;
    }


}
