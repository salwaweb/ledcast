<?php

namespace LedcastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="LedcastBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer")
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="pitchp", type="string", length=255)
     */
    private $pitchp;

    /**
     * @var string
     *
     * @ORM\Column(name="typeled", type="string", length=255)
     */
    private $typeled;

    /**
     * @var string
     *
     * @ORM\Column(name="taille", type="string", length=255)
     */
    private $taille;

    /**
     * @var string
     *
     * @ORM\Column(name="resolution", type="string", length=255)
     */
    private $resolution;

    /**
     * @var string
     *
     * @ORM\Column(name="densite", type="string", length=255)
     */
    private $densite;

    /**
     * @var string
     *
     * @ORM\Column(name="luminosit", type="string", length=255)
     */
    private $luminosit;

    /**
     * @var string
     *
     * @ORM\Column(name="frequence", type="string", length=255)
     */
    private $frequence;

    /**
     * @var string
     *
     * @ORM\Column(name="dimensions", type="string", length=255)
     */
    private $dimensions;

    /**
     * @var string
     *
     * @ORM\Column(name="resolutiond", type="string", length=255)
     */
    private $resolutiond;

    /**
     * @var string
     *
     * @ORM\Column(name="chassis", type="string", length=255)
     */
    private $chassis;

    /**
     * @var string
     *
     * @ORM\Column(name="maintenance", type="string", length=255)
     */
    private $maintenance;

    /**
     * @var string
     *
     * @ORM\Column(name="poids", type="string", length=255)
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="consommation", type="string", length=255)
     */
    private $consommation;

    /**
     * @var string
     *
     * @ORM\Column(name="anglevh", type="string", length=255)
     */
    private $anglevh;

    /**
     * @var string
     *
     * @ORM\Column(name="distancem", type="string", length=255)
     */
    private $distancem;

    /**
     * @var string
     *
     * @ORM\Column(name="carterec", type="string", length=255)
     */
    private $carterec;

    /**
     * @var string
     *
     * @ORM\Column(name="temperateur", type="string", length=255)
     */
    private $temperateur;

    /**
     * @var string
     *
     * @ORM\Column(name="tolerance", type="string", length=255)
     */
    private $tolerance;

    /**
     * @var string
     *
     * @ORM\Column(name="refroidissement", type="string", length=255)
     */
    private $refroidissement;

    /**
     * @var string
     *
     * @ORM\Column(name="indicepro", type="string", length=255)
     */
    private $indicepro;
    /**
     * @var string
     *
     * @ORM\Column(name="Connections", type="string", length=255)
     */
    private $Connections;

    /**
     * @var string
     *
     * @ORM\Column(name="certification", type="string", length=255)
     */
    private $certification;

    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=255)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="garantie", type="string", length=255)
     */
    private $garantie;

    /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Catalogue")
     * @ORM\JoinColumn(nullable=false)
     */

    private $categorie;
    /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     */
     private $type;

   /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Pitch")
     * @ORM\JoinColumn(nullable=false)
     */
     private $pitch;

    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image1;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image2;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image3;
    /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Pointfort")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fort;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Produit
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return Produit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

   

    /**
     * Set typeled
     *
     * @param string $typeled
     *
     * @return Produit
     */
    public function setTypeled($typeled)
    {
        $this->typeled = $typeled;

        return $this;
    }

    /**
     * Get typeled
     *
     * @return string
     */
    public function getTypeled()
    {
        return $this->typeled;
    }

    /**
     * Set taille
     *
     * @param string $taille
     *
     * @return Produit
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * Get taille
     *
     * @return string
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Set resolution
     *
     * @param string $resolution
     *
     * @return Produit
     */
    public function setResolution($resolution)
    {
        $this->resolution = $resolution;

        return $this;
    }

    /**
     * Get resolution
     *
     * @return string
     */
    public function getResolution()
    {
        return $this->resolution;
    }

    /**
     * Set densite
     *
     * @param string $densite
     *
     * @return Produit
     */
    public function setDensite($densite)
    {
        $this->densite = $densite;

        return $this;
    }

    /**
     * Get densite
     *
     * @return string
     */
    public function getDensite()
    {
        return $this->densite;
    }

    /**
     * Set luminosit
     *
     * @param string $luminosit
     *
     * @return Produit
     */
    public function setLuminosit($luminosit)
    {
        $this->luminosit = $luminosit;

        return $this;
    }

    /**
     * Get luminosit
     *
     * @return string
     */
    public function getLuminosit()
    {
        return $this->luminosit;
    }

    /**
     * Set frequence
     *
     * @param string $frequence
     *
     * @return Produit
     */
    public function setFrequence($frequence)
    {
        $this->frequence = $frequence;

        return $this;
    }

    /**
     * Get frequence
     *
     * @return string
     */
    public function getFrequence()
    {
        return $this->frequence;
    }

    /**
     * Set dimensions
     *
     * @param string $dimensions
     *
     * @return Produit
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;

        return $this;
    }

    /**
     * Get dimensions
     *
     * @return string
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * Set resolutiond
     *
     * @param string $resolutiond
     *
     * @return Produit
     */
    public function setResolutiond($resolutiond)
    {
        $this->resolutiond = $resolutiond;

        return $this;
    }

    /**
     * Get resolutiond
     *
     * @return string
     */
    public function getResolutiond()
    {
        return $this->resolutiond;
    }

    /**
     * Set chassis
     *
     * @param string $chassis
     *
     * @return Produit
     */
    public function setChassis($chassis)
    {
        $this->chassis = $chassis;

        return $this;
    }

    /**
     * Get chassis
     *
     * @return string
     */
    public function getChassis()
    {
        return $this->chassis;
    }

    /**
     * Set maintenance
     *
     * @param string $maintenance
     *
     * @return Produit
     */
    public function setMaintenance($maintenance)
    {
        $this->maintenance = $maintenance;

        return $this;
    }

    /**
     * Get maintenance
     *
     * @return string
     */
    public function getMaintenance()
    {
        return $this->maintenance;
    }

    /**
     * Set poids
     *
     * @param string $poids
     *
     * @return Produit
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return string
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set consommation
     *
     * @param string $consommation
     *
     * @return Produit
     */
    public function setConsommation($consommation)
    {
        $this->consommation = $consommation;

        return $this;
    }

    /**
     * Get consommation
     *
     * @return string
     */
    public function getConsommation()
    {
        return $this->consommation;
    }

    /**
     * Set anglevh
     *
     * @param string $anglevh
     *
     * @return Produit
     */
    public function setAnglevh($anglevh)
    {
        $this->anglevh = $anglevh;

        return $this;
    }

    /**
     * Get anglevh
     *
     * @return string
     */
    public function getAnglevh()
    {
        return $this->anglevh;
    }

    /**
     * Set distancem
     *
     * @param string $distancem
     *
     * @return Produit
     */
    public function setDistancem($distancem)
    {
        $this->distancem = $distancem;

        return $this;
    }

    /**
     * Get distancem
     *
     * @return string
     */
    public function getDistancem()
    {
        return $this->distancem;
    }

    /**
     * Set carterec
     *
     * @param string $carterec
     *
     * @return Produit
     */
    public function setCarterec($carterec)
    {
        $this->carterec = $carterec;

        return $this;
    }

    /**
     * Get carterec
     *
     * @return string
     */
    public function getCarterec()
    {
        return $this->carterec;
    }

    /**
     * Set temperateur
     *
     * @param string $temperateur
     *
     * @return Produit
     */
    public function setTemperateur($temperateur)
    {
        $this->temperateur = $temperateur;

        return $this;
    }

    /**
     * Get temperateur
     *
     * @return string
     */
    public function getTemperateur()
    {
        return $this->temperateur;
    }

    /**
     * Set tolerance
     *
     * @param string $tolerance
     *
     * @return Produit
     */
    public function setTolerance($tolerance)
    {
        $this->tolerance = $tolerance;

        return $this;
    }

    /**
     * Get tolerance
     *
     * @return string
     */
    public function getTolerance()
    {
        return $this->tolerance;
    }

    /**
     * Set refroidissement
     *
     * @param string $refroidissement
     *
     * @return Produit
     */
    public function setRefroidissement($refroidissement)
    {
        $this->refroidissement = $refroidissement;

        return $this;
    }

    /**
     * Get refroidissement
     *
     * @return string
     */
    public function getRefroidissement()
    {
        return $this->refroidissement;
    }

    /**
     * Set indicepro
     *
     * @param string $indicepro
     *
     * @return Produit
     */
    public function setIndicepro($indicepro)
    {
        $this->indicepro = $indicepro;

        return $this;
    }

    /**
     * Get indicepro
     *
     * @return string
     */
    public function getIndicepro()
    {
        return $this->indicepro;
    }

    /**
     * Set certification
     *
     * @param string $certification
     *
     * @return Produit
     */
    public function setCertification($certification)
    {
        $this->certification = $certification;

        return $this;
    }

    /**
     * Get certification
     *
     * @return string
     */
    public function getCertification()
    {
        return $this->certification;
    }

    /**
     * Set duree
     *
     * @param string $duree
     *
     * @return Produit
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return string
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set garantie
     *
     * @param string $garantie
     *
     * @return Produit
     */
    public function setGarantie($garantie)
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * Get garantie
     *
     * @return string
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    /**
     * Set categorie
     *
     * @param \LedcastBundle\Entity\Catalogue $categorie
     *
     * @return Produit
     */
    public function setCategorie(\LedcastBundle\Entity\Catalogue $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \LedcastBundle\Entity\Catalogue
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set type
     *
     * @param \LedcastBundle\Entity\Type $type
     *
     * @return Produit
     */
    public function setType(\LedcastBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \LedcastBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pitch
     *
     * @param \LedcastBundle\Entity\Pitch $pitch
     *
     * @return Produit
     */
    public function setPitch(\LedcastBundle\Entity\Pitch $pitch)
    {
        $this->pitch = $pitch;

        return $this;
    }

    /**
     * Get pitch
     *
     * @return \LedcastBundle\Entity\Pitch
     */
    public function getPitch()
    {
        return $this->pitch;
    }

    /**
     * Set image1
     *
     * @param \LedcastBundle\Entity\Media $image1
     *
     * @return Produit
     */
    public function setImage1(\LedcastBundle\Entity\Media $image1 = null)
    {
        $this->image1 = $image1;

        return $this;
    }

    /**
     * Get image1
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * Set image2
     *
     * @param \LedcastBundle\Entity\Media $image2
     *
     * @return Produit
     */
    public function setImage2(\LedcastBundle\Entity\Media $image2 = null)
    {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get image2
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set image3
     *
     * @param \LedcastBundle\Entity\Media $image3
     *
     * @return Produit
     */
    public function setImage3(\LedcastBundle\Entity\Media $image3 = null)
    {
        $this->image3 = $image3;

        return $this;
    }

    /**
     * Get image3
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getImage3()
    {
        return $this->image3;
    }



    /**
     * Set pitchp
     *
     * @param string $pitchp
     *
     * @return Produit
     */
    public function setPitchp($pitchp)
    {
        $this->pitchp = $pitchp;

        return $this;
    }

    /**
     * Get pitchp
     *
     * @return string
     */
    public function getPitchp()
    {
        return $this->pitchp;
    }

    /**
     * Set connections
     *
     * @param string $connections
     *
     * @return Produit
     */
    public function setConnections($connections)
    {
        $this->Connections = $connections;

        return $this;
    }

    /**
     * Get connections
     *
     * @return string
     */
    public function getConnections()
    {
        return $this->Connections;
    }
    public function __toString(){

        return $this->nom;

    }

    /**
     * Set fort
     *
     * @param \LedcastBundle\Entity\Pointfort $fort
     *
     * @return Produit
     */
    public function setFort(\LedcastBundle\Entity\Pointfort $fort)
    {
        $this->fort = $fort;

        return $this;
    }

    /**
     * Get fort
     *
     * @return \LedcastBundle\Entity\Pointfort
     */
    public function getFort()
    {
        return $this->fort;
    }
}
