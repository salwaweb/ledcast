<?php

namespace LedcastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Realisation
 *
 * @ORM\Table(name="realisation")
 * @ORM\Entity(repositoryClass="LedcastBundle\Repository\RealisationRepository")
 */
class Realisation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
     /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255)
     */
    private $lien;
    /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Categorie", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */

    private $Categorie;


    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $media;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Realisation
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Realisation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

/**
     * Set lien
     *
     * @param string $lien
     *
     * @return Realisation
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set categorie
     *
     * @param \LedcastBundle\Entity\Categorie $categorie
     *
     * @return Realisation
     */
    public function setCategorie(\LedcastBundle\Entity\Categorie $categorie)
    {
        $this->Categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \LedcastBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->Categorie;
    }




    /**
     * Set media
     *
     * @param \LedcastBundle\Entity\Media $media
     *
     * @return Realisation
     */
    public function setMedia(\LedcastBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }
}
