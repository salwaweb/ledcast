<?php

namespace LedcastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telechargement
 *
 * @ORM\Table(name="telechargement")
 * @ORM\Entity(repositoryClass="LedcastBundle\Repository\TelechargementRepository")
 */
class Telechargement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fichier;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Telechargement
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set fichier
     *
     * @param \LedcastBundle\Entity\Media $fichier
     *
     * @return Telechargement
     */
    public function setFichier(\LedcastBundle\Entity\Media $fichier = null)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getFichier()
    {
        return $this->fichier;
    }
}
