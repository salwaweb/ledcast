<?php

namespace LedcastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AccessoirPointfort
 *
 * @ORM\Table(name="accessoir_pointfort")
 * @ORM\Entity(repositoryClass="LedcastBundle\Repository\AccessoirRepository")
 */
class AccessoirPointfort
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Accessoir")
     * @ORM\JoinColumn(nullable=false)
     */

    private $accessoir;
    /**
     * @ORM\ManyToOne(targetEntity="LedcastBundle\Entity\Pointfort")
     * @ORM\JoinColumn(nullable=false)
     */

    private $pointfort;



 



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessoir
     *
     * @param \LedcastBundle\Entity\Accessoir $accessoir
     *
     * @return AccessoirPointfort
     */
    public function setAccessoir(\LedcastBundle\Entity\Accessoir $accessoir)
    {
        $this->accessoir = $accessoir;

        return $this;
    }

    /**
     * Get accessoir
     *
     * @return \LedcastBundle\Entity\Accessoir
     */
    public function getAccessoir()
    {
        return $this->accessoir;
    }





    /**
     * Set pointfort
     *
     * @param \LedcastBundle\Entity\Pointfort $pointfort
     *
     * @return AccessoirPointfort
     */
    public function setPointfort(\LedcastBundle\Entity\Pointfort $pointfort)
    {
        $this->pointfort = $pointfort;

        return $this;
    }

    /**
     * Get pointfort
     *
     * @return \LedcastBundle\Entity\Pointfort
     */
    public function getPointfort()
    {
        return $this->pointfort;
    }
}
