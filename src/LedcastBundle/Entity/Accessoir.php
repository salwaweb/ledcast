<?php

namespace LedcastBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Accessoir
 *
 * @ORM\Table(name="accessoir")
 * @ORM\Entity(repositoryClass="LedcastBundle\Repository\AccessoirRepository")
 */
class Accessoir
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="point1", type="string", length=255)
     */
    private $point1;

    /**
     * @var string
     *
     * @ORM\Column(name="point2", type="string", length=255)
     */
    private $point2;

    /**
     * @var string
     *
     * @ORM\Column(name="point3", type="string", length=255)
     */
    private $point3;

    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image1;

    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $image2;

    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fiche;

    /**
     * @ORM\OneToMany(targetEntity="LedcastBundle\Entity\Pointfort", mappedBy="pointf")
     * @ORM\JoinColumn(nullable=true)
     */

    private $access;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Accessoir
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Accessoir
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set point1
     *
     * @param string $point1
     *
     * @return Accessoir
     */
    public function setPoint1($point1)
    {
        $this->point1 = $point1;

        return $this;
    }

    /**
     * Get point1
     *
     * @return string
     */
    public function getPoint1()
    {
        return $this->point1;
    }

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->access = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set point2
     *
     * @param string $point2
     *
     * @return Accessoir
     */
    public function setPoint2($point2)
    {
        $this->point2 = $point2;

        return $this;
    }

    /**
     * Get point2
     *
     * @return string
     */
    public function getPoint2()
    {
        return $this->point2;
    }

    /**
     * Set point3
     *
     * @param string $point3
     *
     * @return Accessoir
     */
    public function setPoint3($point3)
    {
        $this->point3 = $point3;

        return $this;
    }

    /**
     * Get point3
     *
     * @return string
     */
    public function getPoint3()
    {
        return $this->point3;
    }

    /**
     * Set image1
     *
     * @param \LedcastBundle\Entity\Media $image1
     *
     * @return Accessoir
     */
    public function setImage1(\LedcastBundle\Entity\Media $image1 = null)
    {
        $this->image1 = $image1;

        return $this;
    }

    /**
     * Get image1
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * Set image2
     *
     * @param \LedcastBundle\Entity\Media $image2
     *
     * @return Accessoir
     */
    public function setImage2(\LedcastBundle\Entity\Media $image2 = null)
    {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get image2
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set fiche
     *
     * @param \LedcastBundle\Entity\Media $fiche
     *
     * @return Accessoir
     */
    public function setFiche(\LedcastBundle\Entity\Media $fiche = null)
    {
        $this->fiche = $fiche;

        return $this;
    }

    /**
     * Get fiche
     *
     * @return \LedcastBundle\Entity\Media
     */
    public function getFiche()
    {
        return $this->fiche;
    }

    /**
     * Add access
     *
     * @param \LedcastBundle\Entity\Pointfort $access
     *
     * @return Accessoir
     */
    public function addAccess(\LedcastBundle\Entity\Pointfort $access)
    {
        $this->access[] = $access;

        return $this;
    }

    /**
     * Remove access
     *
     * @param \LedcastBundle\Entity\Pointfort $access
     */
    public function removeAccess(\LedcastBundle\Entity\Pointfort $access)
    {
        $this->access->removeElement($access);
    }

    /**
     * Get access
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccess()
    {
        return $this->access;
    }
}
